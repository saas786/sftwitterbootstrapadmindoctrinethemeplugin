<?php $list = $this->getUrlForAction('list') ?>

<div class="pagination pagination-centered">
    <ul>
        <li class="[?php echo ($pager->getPage() === 1) ? 'disabled' : '' ?]"><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=1">&laquo;</a></li>
        <li class="[?php echo ($pager->getPage() === 1) ? 'disabled' : '' ?]"><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=[?php echo $pager->getPreviousPage() ?]">&lsaquo;</a></li>
        [?php foreach ($pager->getLinks() as $page): ?]
            [?php if ($page == $pager->getPage()): ?]
                <li class="active"><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=[?php echo $page ?]">[?php echo $page ?]</a></li>
            [?php else: ?]
                <li><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=[?php echo $page ?]">[?php echo $page ?]</a></li>
            [?php endif; ?]
        [?php endforeach; ?]
        <li class="[?php echo ($pager->getPage() == $pager->getLastPage()) ? 'disabled' : '' ?]"><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=[?php echo $pager->getNextPage() ?]">&rsaquo;</a></li>
        <li class="[?php echo ($pager->getPage() == $pager->getLastPage()) ? 'disabled' : '' ?]"><a href="[?php echo url_for('@<?php echo $list ?>') ?]?page=[?php echo $pager->getLastPage() ?]">&raquo;</a></li>
    </ul>
</div>
